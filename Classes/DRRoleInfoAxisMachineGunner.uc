
class DRRoleInfoAxisMachineGunner extends DRRoleInfoAxis;

DefaultProperties
{
	RoleType=RORIT_MachineGunner
	ClassTier=2
	ClassIndex=`RI_MACHINE_GUNNER
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'ROWeap_MG34_LMG')
	)}
	
	bAllowPistolsInRealism=true
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_mg'
}
