
class DRRoleInfoAxisRifleman extends DRRoleInfoAxis;

DefaultProperties
{
	RoleType=RORIT_Rifleman
	ClassTier=1
	ClassIndex=`RI_RIFLEMAN
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'DRWeapon_Kar98k'),
		
		OtherItems=(class'ROWeap_Type67_GrenadeSingle')
	)}
	
	// ClassIcon=
}
