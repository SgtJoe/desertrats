
class DRAmmo_Webley extends ROAmmunition
	abstract;

defaultproperties
{
	CompatibleWeaponClasses(0)=class'DRWeapon_Webley'
	
	InitialAmount=6
	Weight=0.1
	ClipsPerSlot=6
}
