
class DRRoleInfoAxisSniper extends DRRoleInfoAxis;

DefaultProperties
{
	RoleType=RORIT_Marksman
	ClassTier=3
	ClassIndex=`RI_SNIPER
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'ROWeap_MN9130Scoped_Rifle_Content')
	)}
	
	bAllowPistolsInRealism=true
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_sniper'
}
