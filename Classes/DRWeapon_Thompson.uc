
class DRWeapon_Thompson extends ROWeap_M1A1_SMG
	abstract;

`include(DesertRats\Classes\DRWeaponPickupMessagesOverride.uci)

defaultproperties
{
	WeaponContentClass(0)="DesertRats.DRWeapon_Thompson_ActualContent"
	
	InvIndex=`WI_THOMPSON
}
