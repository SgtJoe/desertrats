
class DRRoleInfoAlliesSniper extends DRRoleInfoAllies;

DefaultProperties
{
	RoleType=RORIT_Marksman
	ClassTier=3
	ClassIndex=`RI_SNIPER
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'ROWeap_M40Scoped_Rifle')
	)}
	
	bAllowPistolsInRealism=true
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_sniper'
}
