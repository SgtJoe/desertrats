
class DRWeapon_MP40 extends ROWeap_MP40_SMG
	abstract;

`include(DesertRats\Classes\DRWeaponPickupMessagesOverride.uci)

defaultproperties
{
	WeaponContentClass(0)="DesertRats.DRWeapon_MP40_ActualContent"
	
	InvIndex=`WI_MP40
}
