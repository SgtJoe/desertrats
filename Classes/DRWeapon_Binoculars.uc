
class DRWeapon_Binoculars extends ROItem_Binoculars
	abstract;

`include(DesertRats\Classes\DRWeaponPickupMessagesOverride.uci)

DefaultProperties
{
	WeaponContentClass(0)="DesertRats.DRWeapon_Binoculars_ActualContent"
	
	InvIndex=`WI_BINOCS
}
