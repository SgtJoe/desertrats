
class DRRoleInfoAxisAssault extends DRRoleInfoAxis;

DefaultProperties
{
	RoleType=RORIT_Scout
	ClassTier=2
	ClassIndex=`RI_ASSAULT
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'DRWeapon_MP40'),
		
		OtherItems=(class'ROWeap_Type67_Grenade',class'ROWeap_RDG1_Smoke')
	)}
	
	// ClassIcon=
}
